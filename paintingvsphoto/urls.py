from django.urls import path

from paintingvsphoto import views

urlpatterns = [
    path('', views.index, name='index'),
    path('predict/', views.predict, name='predict'),
]
