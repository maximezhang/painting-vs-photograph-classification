import os

import requests

from django.shortcuts import render

from classification import settings

PREDICTION_URL = os.getenv('PREDICTION_URL', 'http://localhost:8080')
PREDICTION_KEY = os.getenv('PREDICTION_KEY')


def index(request):
    return render(request, 'index.html')


def predict(request):
    if 'url' in request.POST:
        url = PREDICTION_URL + '/url'
        payload = {'url': request.POST['url']}
        response = requests.post(url, json=payload, headers={'prediction-key': PREDICTION_KEY})
    else:
        url = PREDICTION_URL + '/image'
        payload = request.FILES['image'].read()
        response = requests.post(url, data=payload, headers={'prediction-key': PREDICTION_KEY})

        with open(os.path.join(settings.MEDIA_ROOT, 'image.jpg'), 'wb') as f:
            f.write(payload)

    painting = round(response.json()['predictions'][0]['probability'] * 100, 1)
    photo = round(response.json()['predictions'][1]['probability'] * 100, 1)

    context = {
        'is_painting': painting > photo,
        'painting': painting if painting > 1 else 'less than 1',
        'photo': photo if photo > 1 else 'less than 1',
    }

    if 'url' in request.POST:
        context.update(payload)

    return render(request, 'index.html', context)
