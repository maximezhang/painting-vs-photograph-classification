FROM python:3.7-slim
ENV PYTHONUNBUFFERED 1
WORKDIR /usr/src/app
COPY classification classification
COPY paintingvsphoto paintingvsphoto
COPY manage.py .
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
CMD python manage.py runserver 0.0.0.0:8000
