# Painting Vs Photograph Classification

Web application for painting versus photo classification.

## Prerequisite

- [Docker](https://docs.docker.com/get-docker/)

## Run the project

```shell script
$ docker-compose up
```

The website is accessible [here](http://localhost:8000/).

# Authors

- Maxime Zhang, <maxime.zhang@epita.fr>
- David Chen, <david.chen@epita.fr>
- Ivan Lin, <ivan.lin@epita.fr>
- Vincent Der, <vincent.der@epita.fr>
- Arnaud Ly, <arnaud.ly@epita.fr>
